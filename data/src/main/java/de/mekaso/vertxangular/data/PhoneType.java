package de.mekaso.vertxangular.data;

public enum PhoneType {
	Mobile, Landline
}
