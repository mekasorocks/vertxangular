/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
export class Address {
    /*private*/ street : string;

    /*private*/ zip : string;

    /*private*/ city : string;

    /*private*/ state : string;

    constructor(builder : Address.Builder) {
        if(this.street===undefined) this.street = null;
        if(this.zip===undefined) this.zip = null;
        if(this.city===undefined) this.city = null;
        if(this.state===undefined) this.state = null;
        this.street = builder.street;
        this.zip = builder.zip;
        this.city = builder.city;
        this.state = builder.state;
    }

    /**
     * @return {string} the street
     */
    public getStreet() : string {
        return this.street;
    }

    /**
     * @param {string} street the street to set
     */
    public setStreet(street : string) {
        this.street = street;
    }

    /**
     * @return {string} the zip
     */
    public getZip() : string {
        return this.zip;
    }

    /**
     * @param {string} zip the zip to set
     */
    public setZip(zip : string) {
        this.zip = zip;
    }

    /**
     * @return {string} the city
     */
    public getCity() : string {
        return this.city;
    }

    /**
     * @param {string} city the city to set
     */
    public setCity(city : string) {
        this.city = city;
    }

    /**
     * @return {string} the state
     */
    public getState() : string {
        return this.state;
    }

    /**
     * @param {string} state the state to set
     */
    public setState(state : string) {
        this.state = state;
    }

    /**
     * Creates builder to build {@link Address}.
     * @return {Address.Builder} created builder
     */
    public static builder() : Address.Builder {
        return new Address.Builder();
    }
}
Address["__class"] = "de.mekaso.vertxangular.data.Address";


export namespace Address {

    /**
     * Builder to build {@link Address}.
     * @class
     */
    export class Builder {
        street : string;

        zip : string;

        city : string;

        state : string;

        constructor() {
            if(this.street===undefined) this.street = null;
            if(this.zip===undefined) this.zip = null;
            if(this.city===undefined) this.city = null;
            if(this.state===undefined) this.state = null;
        }

        public withStreet(street : string) : Address.Builder {
            this.street = street;
            return this;
        }

        public withZip(zip : string) : Address.Builder {
            this.zip = zip;
            return this;
        }

        public withCity(city : string) : Address.Builder {
            this.city = city;
            return this;
        }

        public withState(state : string) : Address.Builder {
            this.state = state;
            return this;
        }

        public build() : Address {
            return new Address(this);
        }
    }
    Builder["__class"] = "de.mekaso.vertxangular.data.Address.Builder";

}



