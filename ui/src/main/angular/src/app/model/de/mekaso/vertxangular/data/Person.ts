/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
import { Address } from './Address';
import { Phone } from './Phone';

export class Person {
    /*private*/ name : string;

    /*private*/ firstName : string;

    /*private*/ birthday : Date;

    /*private*/ address : Address;

    /*private*/ phones : Array<Phone>;

    constructor(builder : Person.Builder) {
        if(this.name===undefined) this.name = null;
        if(this.firstName===undefined) this.firstName = null;
        if(this.birthday===undefined) this.birthday = null;
        if(this.address===undefined) this.address = null;
        if(this.phones===undefined) this.phones = null;
        this.name = builder.name;
        this.firstName = builder.firstName;
        this.birthday = builder.birthday;
        this.address = builder.address;
        this.phones = builder.phones;
    }

    /**
     * @return {string} the name
     */
    public getName() : string {
        return this.name;
    }

    /**
     * @param {string} name the name to set
     */
    public setName(name : string) {
        this.name = name;
    }

    /**
     * @return {string} the firstName
     */
    public getFirstName() : string {
        return this.firstName;
    }

    /**
     * @param {string} firstName the firstName to set
     */
    public setFirstName(firstName : string) {
        this.firstName = firstName;
    }

    /**
     * @return {Date} the birthday
     */
    public getBirthday() : Date {
        return this.birthday;
    }

    /**
     * @param {Date} birthday the birthday to set
     */
    public setBirthday(birthday : Date) {
        this.birthday = birthday;
    }

    /**
     * @return {Address} the address
     */
    public getAddress() : Address {
        return this.address;
    }

    /**
     * @param {Address} address the address to set
     */
    public setAddress(address : Address) {
        this.address = address;
    }

    /**
     * @return {Phone[]} the phones
     */
    public getPhones() : Array<Phone> {
        return this.phones;
    }

    /**
     * @param {Phone[]} phones the phones to set
     */
    public setPhones(phones : Array<Phone>) {
        this.phones = phones;
    }

    /**
     * Creates builder to build {@link Person}.
     * @return {Person.Builder} created builder
     */
    public static builder() : Person.Builder {
        return new Person.Builder();
    }
}
Person["__class"] = "de.mekaso.vertxangular.data.Person";


export namespace Person {

    /**
     * Builder to build {@link Person}.
     * @class
     */
    export class Builder {
        name : string;

        firstName : string;

        birthday : Date;

        address : Address;

        phones : Array<Phone> = /* emptyList */[];

        constructor() {
            if(this.name===undefined) this.name = null;
            if(this.firstName===undefined) this.firstName = null;
            if(this.birthday===undefined) this.birthday = null;
            if(this.address===undefined) this.address = null;
        }

        public withName(name : string) : Person.Builder {
            this.name = name;
            return this;
        }

        public withFirstName(firstName : string) : Person.Builder {
            this.firstName = firstName;
            return this;
        }

        public withBirthday(birthday : Date) : Person.Builder {
            this.birthday = birthday;
            return this;
        }

        public withAddress(address : Address) : Person.Builder {
            this.address = address;
            return this;
        }

        public withPhones(phones : Array<Phone>) : Person.Builder {
            this.phones = phones;
            return this;
        }

        public build() : Person {
            return new Person(this);
        }
    }
    Builder["__class"] = "de.mekaso.vertxangular.data.Person.Builder";

}



