package de.mekaso.vertxangular.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import de.mekaso.vertxangular.data.Address;
import de.mekaso.vertxangular.data.Person;
import de.mekaso.vertxangular.data.Phone;
import de.mekaso.vertxangular.data.PhoneType;

public class PersonServiceImpl {

	private List<Person> allPersons;
	
	public PersonServiceImpl() {
		this.allPersons = new ArrayList<>();
		Person person = Person.builder()
				.withName("Simpson")
				.withFirstName("Josh")
				.withBirthday(new Date())
				.withAddress(Address.builder()
						.withCity("Birmingham")
						.withZip("82788")
						.withState("New York")
						.withStreet("Main Street 92")
						.build())
				.withPhones(Arrays.asList(Phone.builder()
						.withNumber("02677 - 92788000")
						.withPhonetype(PhoneType.Landline)
						.build())
						)
				.build();
		allPersons.add(person);
		person = Person.builder()
				.withName("Mayer")
				.withFirstName("Gwen")
				.withBirthday(new Date())
				.withAddress(Address.builder()
						.withCity("Nottingham")
						.withZip("12638")
						.withState("California")
						.withStreet("Center Station 6")
						.build())
				.withPhones(Arrays.asList(Phone.builder()
						.withNumber("02691 - 12345000")
						.withPhonetype(PhoneType.Mobile)
						.build())
						)
				.build();
		allPersons.add(person);
	}
	
	public List<Person> getAll() {
		return this.allPersons;
	}
}
