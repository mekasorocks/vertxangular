package de.mekaso.vertxangular.data;

import javax.annotation.Generated;

public class Phone {

	private PhoneType phonetype;
	private String number;
	@Generated("SparkTools")
	private Phone(Builder builder) {
		this.phonetype = builder.phonetype;
		this.number = builder.number;
	}
	/**
	 * @return the phonetype
	 */
	public PhoneType getPhonetype() {
		return phonetype;
	}
	/**
	 * @param phonetype the phonetype to set
	 */
	public void setPhonetype(PhoneType phonetype) {
		this.phonetype = phonetype;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * Creates builder to build {@link Phone}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}
	/**
	 * Builder to build {@link Phone}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private PhoneType phonetype;
		private String number;

		private Builder() {
		}

		public Builder withPhonetype(PhoneType phonetype) {
			this.phonetype = phonetype;
			return this;
		}

		public Builder withNumber(String number) {
			this.number = number;
			return this;
		}

		public Phone build() {
			return new Phone(this);
		}
	}
}
