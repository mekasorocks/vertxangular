# vertxangular

Proof of concept with Java, Vert.x, Angular, Java to Typescript

some design goals are:

- reuse domain model java classes in javascript frontend
- lightweight frontend with javascript UI inside a self-contained system
- lightweight non-blocking backend with vert.x