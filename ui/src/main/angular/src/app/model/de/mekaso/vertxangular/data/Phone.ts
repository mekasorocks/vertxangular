/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
import { PhoneType } from './PhoneType';

export class Phone {
    /*private*/ phonetype : PhoneType;

    /*private*/ number : string;

    constructor(builder : Phone.Builder) {
        if(this.phonetype===undefined) this.phonetype = null;
        if(this.number===undefined) this.number = null;
        this.phonetype = builder.phonetype;
        this.number = builder.number;
    }

    /**
     * @return {PhoneType} the phonetype
     */
    public getPhonetype() : PhoneType {
        return this.phonetype;
    }

    /**
     * @param {PhoneType} phonetype the phonetype to set
     */
    public setPhonetype(phonetype : PhoneType) {
        this.phonetype = phonetype;
    }

    /**
     * @return {string} the number
     */
    public getNumber() : string {
        return this.number;
    }

    /**
     * @param {string} number the number to set
     */
    public setNumber(number : string) {
        this.number = number;
    }

    /**
     * Creates builder to build {@link Phone}.
     * @return {Phone.Builder} created builder
     */
    public static builder() : Phone.Builder {
        return new Phone.Builder();
    }
}
Phone["__class"] = "de.mekaso.vertxangular.data.Phone";


export namespace Phone {

    /**
     * Builder to build {@link Phone}.
     * @class
     */
    export class Builder {
        phonetype : PhoneType;

        number : string;

        constructor() {
            if(this.phonetype===undefined) this.phonetype = null;
            if(this.number===undefined) this.number = null;
        }

        public withPhonetype(phonetype : PhoneType) : Phone.Builder {
            this.phonetype = phonetype;
            return this;
        }

        public withNumber(number : string) : Phone.Builder {
            this.number = number;
            return this;
        }

        public build() : Phone {
            return new Phone(this);
        }
    }
    Builder["__class"] = "de.mekaso.vertxangular.data.Phone.Builder";

}



