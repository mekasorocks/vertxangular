package de.mekaso.vertxangular.web;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class Webserver extends AbstractVerticle {
	private static Logger LOGGER = LoggerFactory.getLogger(Webserver.class);
	private static final String CONTENT_TYPE = "application/json; charset=utf-8";
	private static final int DEFAULT_PORT_NO = 80;
	
	private HttpServer server;
	
	private PersonServiceImpl personService;

	/**
	 * Just to start the webserver within an IDE.
	 * @param args command line args
	 */
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(Webserver.class.getName());
	}

	public void start(Future<Void> startFuture) throws Exception {
		LOGGER.info("starting...");
		this.server = this.vertx.createHttpServer();
		Router router = Router.router(vertx);
		// rest service methods
		router.get("/api/persons").handler(this::addRouteGetAll);
		// static web resources (UI files, html, js...)
		router.route().handler(StaticHandler.create().setCachingEnabled(false));
		router.route().handler(CorsHandler.create(".*"));
		int portNo = this.config().getInteger("server.port", DEFAULT_PORT_NO);
		server.requestHandler(router::accept).listen(portNo, asyncResult -> {
			if (asyncResult.succeeded()) {
				this.personService = new PersonServiceImpl();
				registerJsonModules();
				LOGGER.info("server is started on port {0}", portNo);
				startFuture.complete();
			} else {
				LOGGER.error("server could not be startet on port {0}", portNo, asyncResult.cause());
				startFuture.fail(asyncResult.cause());
			}
		});
	}
	
	private void registerJsonModules() {
		// special module for Java8 date time classes
		JavaTimeModule module = new JavaTimeModule();
		module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ISO_DATE_TIME));
		Json.mapper.registerModule(module).setTimeZone(TimeZone.getTimeZone("UTC"));
		Json.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		Json.prettyMapper.registerModule(module).setTimeZone(TimeZone.getTimeZone("UTC"));
		Json.prettyMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}
	
	private void addRouteGetAll(RoutingContext routingContext) {
		routingContext.response()
	      .putHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
	      .putHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
	      .end(Json.encodePrettily(this.personService.getAll()));
	}

	public void stop(Future<Void> stopFuture) {
		stopFuture.complete();
	}
}
