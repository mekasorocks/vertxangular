package de.mekaso.vertxangular.data;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Generated;

public class Person {
	private String name;
	private String firstName;
	private Date birthday;
	private Address address;
	private List<Phone> phones;


	@Generated("SparkTools")
	private Person(Builder builder) {
		this.name = builder.name;
		this.firstName = builder.firstName;
		this.birthday = builder.birthday;
		this.address = builder.address;
		this.phones = builder.phones;
	}
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}
	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	/**
	 * @return the phones
	 */
	public List<Phone> getPhones() {
		return phones;
	}
	/**
	 * @param phones the phones to set
	 */
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
	/**
	 * Creates builder to build {@link Person}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}
	/**
	 * Builder to build {@link Person}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String name;
		private String firstName;
		private Date birthday;
		private Address address;
		private List<Phone> phones = Collections.emptyList();

		private Builder() {
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder withBirthday(Date birthday) {
			this.birthday = birthday;
			return this;
		}

		public Builder withAddress(Address address) {
			this.address = address;
			return this;
		}

		public Builder withPhones(List<Phone> phones) {
			this.phones = phones;
			return this;
		}

		public Person build() {
			return new Person(this);
		}
	}
}
