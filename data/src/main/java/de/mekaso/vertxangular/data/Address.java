package de.mekaso.vertxangular.data;

import javax.annotation.Generated;

public class Address {
	private String street;
	private String zip;
	private String city;
	private String state;
	@Generated("SparkTools")
	private Address(Builder builder) {
		this.street = builder.street;
		this.zip = builder.zip;
		this.city = builder.city;
		this.state = builder.state;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * Creates builder to build {@link Address}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}
	/**
	 * Builder to build {@link Address}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String street;
		private String zip;
		private String city;
		private String state;

		private Builder() {
		}

		public Builder withStreet(String street) {
			this.street = street;
			return this;
		}

		public Builder withZip(String zip) {
			this.zip = zip;
			return this;
		}

		public Builder withCity(String city) {
			this.city = city;
			return this;
		}

		public Builder withState(String state) {
			this.state = state;
			return this;
		}

		public Address build() {
			return new Address(this);
		}
	}
}
