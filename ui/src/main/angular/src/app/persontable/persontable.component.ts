import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Person } from '../model/de/mekaso/vertxangular/data/Person';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-persontable',
  templateUrl: './persontable.component.html',
  styleUrls: ['./persontable.component.css']
})
export class PersontableComponent implements OnInit {
  personList: Person [] = [];
  hidden: boolean = true;
  selectedPerson: Person = null;

  constructor(private httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  ngOnInit() {
    this.httpClient.get<Person []>("/api/persons").subscribe(personList => {
      this.personList = personList;
    }, (errorResponse: HttpErrorResponse) => {
      if (errorResponse.error instanceof Error) {
        console.log("Client-side error occured.");
      } else {
        console.log("Server-side error occured.");
      }
    });
  }

  onRowClicked(person: Person) {
    this.hidden = false;
    this.selectedPerson = person;
  }
}
